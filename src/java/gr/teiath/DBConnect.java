/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teiath;

import java.sql.*;

/**
 *
 * @author Nick
 */
public class DBConnect {
    Connection con = null;
    Statement stmt = null;
    Statement stmt2 = null;
    

    public DBConnect() throws SQLException, ClassNotFoundException {
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        con=DriverManager.getConnection("jdbc:derby://localhost:1527/users","nikos","nikos");
        stmt =con.createStatement();
        stmt2=con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
        
    }
    
    public void executeStatement(String query) throws SQLException{stmt.executeUpdate(query);}
    public String getString(){
        Integer i1 = 0;
        ResultSet rs = null; 
        try{
        rs = stmt.executeQuery("SELECT * FROM USERS");
        while (rs.next()){i1 = i1 + 1;}
        }catch(SQLException se2){
            se2.printStackTrace();}
    return i1.toString();
            }
    public ResultSet getResults(String query){ 
        ResultSet rs = null;
        try{
        rs = stmt2.executeQuery(query);
        }catch(SQLException se2){
            se2.printStackTrace();}
    return rs;
    
    }
   
    
}
