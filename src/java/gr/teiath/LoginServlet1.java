/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teiath;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author mpiuser
 */
@WebServlet(name = "LoginServlet1", urlPatterns = {"/LoginServlet1"})
public class LoginServlet1 extends HttpServlet {
 
    DBConnect db;
    HttpSession session;
    
    

    public LoginServlet1() throws SQLException, ClassNotFoundException {
        this.db = new DBConnect();
    }
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             
             response.setContentType("text/html;charset=UTF-8");
             String email=request.getParameter("email");  
             String password=request.getParameter("password"); 
             String onoma = null; 
             String epitheto = null;
             String mail = null;
             String remember = request.getParameter("remember");
             String apotelesma = "<h1>Δεν έδωσες υπαρκτά στοιχεία</h1>";
             session = request.getSession(false);
              if(session!=null){  
                email=(String)session.getAttribute("email"); 
                password =(String)session.getAttribute("password");
              }
              

             ResultSet rs;
             rs = db.getResults("SELECT * FROM USERS WHERE email = '" + email + "' AND password = '" + password + "'");
             
             try {
                while (rs.next()){
                     onoma = rs.getString("firstname"); epitheto = rs.getString("lastname"); mail = rs.getString("email");
                     apotelesma = "<h1>Καλωσήρθες " + onoma + " " + epitheto + ". To e-mail που έχεις δηλώσει είναι: " + mail +  "</h1>"
                     + "<form action=\"ChangePass\" method=\"post\"> "
                     +"Αλλαγή Κωδικού: <input type=\"password\" name=\"password\"/><br/><br/>  "
                     +"Επαλήθευση Κωδικού: <input type=\"password\" name=\"password2\"/><br/><br/> "
                     +"<input type='hidden' name='email' value='"+email+"'>"
                     +"<input type=\"submit\" value=\"Αλλαγή\"/>  "
                     +"</form>"
                     +"<br><a href='LogOut'>Έξοδος από το σύστημα</a></br>";
                     if (remember != null){
                     Cookie cookie = new Cookie("email",email);
                     cookie.setMaxAge(60*60*24);
                     response.addCookie(cookie);}
                     session=request.getSession();
                     session.setAttribute("email",email); 
                     session.setAttribute("password", password);
                     session.setMaxInactiveInterval(600);
                     
                     if (email.equals("admin                                   ") || email.equals("admin")){
                      apotelesma = "<h1>Καλωσήρθες Administrator</h1>"
                     +"<br><img src='http://scontent.cdninstagram.com/t51.2885-19/s150x150/14240997_178423615898994_1661760173_a.jpg' alt='admin'</br>"
                     + "<form action=\"ChangePass\" method=\"post\"> "
                     +"Αλλαγή Κωδικού: <input type=\"password\" name=\"password\"/><br/><br/>  "
                     +"Επαλήθευση Κωδικού: <input type=\"password\" name=\"password2\"/><br/><br/> "
                     +"<input type='hidden' name='email' value='"+email+"'>"
                     +"<input type=\"submit\" value=\"Αλλαγή\"/>  "
                     +"</form>"
                     +"<br>Διαγραφή Χρηστών</br>"
                     
                     +this.getAllNames()
                     
                     +"<br><a href='LogOut'>Έξοδος από το σύστημα</a></br>";};
                     
                     }
             } catch (SQLException ex) {
                Logger.getLogger(LoginServlet1.class.getName()).log(Level.SEVERE, null, ex);
             }
        try {
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(LoginServlet1.class.getName()).log(Level.SEVERE, null, ex);
        }
             
             
        try (PrintWriter out = response.getWriter()) {
            
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<center>");
            out.println("<head>");
            out.println("<title>Αποτελέσματα Εισόδου στο Σύστημα</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<br><img src='http://www.teiath.gr/images/top_logo_teia_el.jpg' alt='logo'</br>");
            out.println("<h1>Αποτελέσματα Εισόδου στο Σύστημα:</h1>");
            out.println(apotelesma);
            //out.println("<a href='#' onclick='history.go(-1)'>Επιστροφή</a>");
            out.println("</body>");
            out.println("</center>");
            out.println("</html>");
            out.close();
        }
    }
    private String getAllNames() throws SQLException{
        String s1 = "<form action=\"Edit\" method=\"post\"> " + "<select name=\"xristes\">";
        String s2 = null;
        ResultSet rs = db.getResults("SELECT email from USERS");
        while (rs.next()){
         s2 = s2 + " <option value='"+rs.getString("email")+ "'>"+rs.getString("email")+"</option>";}
         s1 = s1 + s2 +
                    "<input type='hidden' name='type' value='delete'>" +
                    "<input type=\"Submit\" name=\"email\"value=\"Διαγραφή\">\n" +
                    "</form>"  ;     
         s1 = s1 +  "<br>Αλλαγή Κωδικού Πρόσβασης Χρηστών</a></br><br></br>"
                    +"<form action=\"Edit\" method=\"post\"> " + "<select name=\"xristes2\">" + s2
                    + "</select>\n" +
                    "<br>Αλλαγή Κωδικού: <input type=\"password\" name=\"password\"/><br/><br/>  "
                    +"Επαλήθευση Κωδικού: <input type=\"password\" name=\"password2\"/><br/><br/> " +
                    "<input type=\"Submit\" name=\"email\"value=\"Αλλαγή\">\n" +
                    "<input type='hidden' name='type' value='edit'>" +
                    "</form>"  ;         
        return s1;}
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            doPost(request, response);
       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
