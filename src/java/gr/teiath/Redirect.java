/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teiath;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Nick
 */
@WebServlet(name = "Redirect", urlPatterns = {"/Redirect"})
public class Redirect extends HttpServlet {
    DBConnect db;

    public Redirect() throws SQLException, ClassNotFoundException {
        this.db = new DBConnect();
    }
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String apotelesma = null;
        Cookie ck[]=request.getCookies();
        ResultSet rs = null;
        if (ck!=null){
             rs = db.getResults("SELECT * FROM USERS WHERE email = '" + ck[0].getValue() + "'");}
        try {if (rs != null){
            while (rs.next()){
               apotelesma = "<h1>Καλωσόρισες " + rs.getString("email") +"</h1>"
                              + "<form action=\"LoginServlet1\" method=\"post\"> "
                              + "<input type='hidden' name='email' value='"+rs.getString("email")+"'>"
                              + "<input type='hidden' name='password' value='"+rs.getString("password")+"'>"
                              +"<input type=\"submit\" value=\"Συνέχεια\"/>  "
                              +"</form>";}
            }else{apotelesma = "<meta http-equiv=\"refresh\" content=\"0; url=login.html\" />";}
      
        } catch (SQLException ex) {
            Logger.getLogger(Redirect.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (PrintWriter out = response.getWriter()) {

            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<center>");
            out.println("<head>");
            out.println("<title>Servlet Redirect</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println(apotelesma);
            out.println("</body>");
            out.println("</center>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
