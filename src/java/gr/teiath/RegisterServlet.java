/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teiath;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Nick
 */
@WebServlet(name = "RegisterServlet", urlPatterns = {"/RegisterServlet"})
public class RegisterServlet extends HttpServlet {
    DBConnect db;
    String s1;

    public RegisterServlet() throws SQLException, ClassNotFoundException {
        this.db = new DBConnect();
        
        
    }
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        s1 = "<h1> <br>Οι κωδικοί πρόσβασης δεν ταιριάζουν,</br>ο χρήστης υπάρχει ήδη, "
                + "ή έχει μείνει κάποιο πεδίο κενό</h1>"+
                "<a href='/WebApplication1/register.html\'>Επιστροφή</a>";
             String onoma=request.getParameter("firstname");  
             String password=request.getParameter("password"); 
             String email=request.getParameter("email");  
             String password2=request.getParameter("password2"); 
             String epitheto=request.getParameter("lastname");
             Boolean b1 = true;
             if (onoma.equals("") || epitheto.equals("") || email.equals("") || password.equals("")){b1 = false;}
             if (password.equals(password2) && b1){
        try {
            db.executeStatement("INSERT INTO USERS VALUES ('" + email + "','" +onoma+ "','" + epitheto + "','" + password + "')"  );
            s1 = "<h1> <br>H εγγραφή</br>ολοκληρώθηκε</h1>"+"<a href='#' onclick='history.go(-1)'>Επιστροφή</a>";
        } catch (SQLException ex) {
            Logger.getLogger(RegisterServlet.class.getName()).log(Level.SEVERE, null, ex);
        }}
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<center>");
            out.println("<head>");
            out.println("<title>Αποτελέσματα Εγγραφής</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println(s1);
            out.println("</body>");
            out.println("</center>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
