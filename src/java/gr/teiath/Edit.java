/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teiath;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Nick
 */
@WebServlet(name = "Edit", urlPatterns = {"/Edit"})
public class Edit extends HttpServlet {
    DBConnect db;

    public Edit() throws SQLException, ClassNotFoundException {
        this.db = new DBConnect();
    }

    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String apotelesma = "Παρακαλώ εισάγετε σωστά τον νέο κωδικό πρόσβασης";
        String password = null;
        String password2 = null;
        String type = request.getParameter("type"); 
        String email=request.getParameter("xristes");  
        String email2 = request.getParameter("xristes2");
        if (email2 != null){
        email2 = email2.replace(" ","");}
        if (type.equals("edit")){
        password=request.getParameter("password"); 
        
        password2=request.getParameter("password2"); 
        }
        if (type.equals("edit") && password.equals(password2) && !password.equals("") && !email2.equals("admin")){
            try {
                db.executeStatement("UPDATE USERS SET password = '" +password+ "' WHERE email = '"+email2+"'");
                apotelesma = "Άλλαξε επιτυχώς ο κωδικός πρόσβασης ";
            } catch (SQLException ex) {
                Logger.getLogger(Edit.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (type.equals("edit") && email2.equals("admin")){
            apotelesma = "Δε μπορεί να αλλάξει ο κωδικός του Administrator";
        }
        
       
        if (type.equals("delete") && !email.equals("admin                                   ")){
            try {
                db.executeStatement("DELETE FROM USERS WHERE email = '" + email + "'");
                apotelesma = "Διεγράφη επιτυχώς ο χρήσης " + email;
            } catch (SQLException ex) {
                Logger.getLogger(Edit.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (type.equals("delete") && email.equals("admin                                   ")){
            apotelesma = "Δε μπορεί να διαγραφεί ο Administrator";
        }
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
                Cookie ck[]=request.getCookies();  

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<center>");
            out.println("<head>");
            out.println("<title>Servlet Edit</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>"+apotelesma+"</h1>");
            out.println("<a href=\"/WebApplication1/LoginServlet1\">Επιστροφή στο προφίλ</a>");
            out.println("</body>");
            out.println("</center>");
            out.println("</html>");
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
